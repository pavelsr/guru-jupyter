# docker build -t pavelsr/guru-jupyter .

FROM jupyter/tensorflow-notebook

COPY requirements.txt .
RUN python -m pip install -r requirements.txt
RUN rm requirements.txt

ENV PYTHONPATH /mypythonlib

WORKDIR /prj
