https://gitlab.com/pavelsr/guru-jupyter/

https://mybinder.org/v2/gl/pavelsr%2Fguru-jupyter/HEAD?filepath=guru.ipynb

jupyter/tensorflow-notebook

# Run

## Local

docker run -p 8888:8888 -v $(pwd):/prj -w /prj pavelsr/guru-jupyter

with: -v /path-to-guru-core:/mypythonlib

docker run -p 8888:8888 -w /prj -v $(pwd):/prj -v $(pwd)/../guru-core:/mypythonlib pavelsr/guru-jupyter



## Production

docker run -d --name guru-jupyter --network nginx-proxy -v $(pwd):/prj -w /prj -e VIRTUAL_HOST=jup.guru.tst.serikoff.pro -e VIRTUAL_PORT=8888 pavelsr/guru-jupyter

# Debug

```
python -c "import sys; print('\n'.join(sys.path))"
docker run pavelsr/guru-jupyter python -c "import sys; print('\n'.join(sys.path))"
docker run pavelsr/guru-jupyter python -c "import os; os.environ['PYTHONPATH'].split(os.pathsep)"
```
