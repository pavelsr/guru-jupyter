#!/usr/bin/env bash

docker stop guru-jupyter
docker rm guru-jupyter
docker run -d --restart always --name guru-jupyter --network nginx-proxy -v $(pwd):/prj -w /prj -e VIRTUAL_HOST=jup.guru.tst.serikoff.pro -e VIRTUAL_PORT=8888 pavelsr/guru-jupyter
docker logs -f guru-jupyter
